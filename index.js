//import thu vien express js 
const express = require('express');

//import thu vien mongoosejs
const mongoose = require("mongoose");

//Khoi tao app Express 
const app = express();

//Khai bao cong chay project
const port = 3005;

//Import router Module
const courseRouter = require("./app/routers/courseRouter");
const reviewRouter = require("./app/routers/reviewRouter");

//Import model module
const reviewModel = require("./app/models/reviewModel");
const courseModel = require("./app/models/courseModel");

app.use(express.json());

app.use((req,res,next) => {
    let today = new Date();

    console.log("Current: ", today);

    next();
})

app.use((req,res,next) =>{
    console.log("Methos: ", req.method);
    
    next();
})

mongoose.connect("mongodb://localhost:27017/CRUD_Course", (error) => {
    if (error) throw error;
    console.log("connect successfully!")
});

//Callback function la mot function tham so cura mot function khac, no se thuc hien khi function day duoc goi
//Khai bao api dang /
app.get("/", (req, res) => {
    let today = new Date();

    res.status(200).json({
        message:  `Xin chao, hom nay la ngay ${today.getDate()} thang ${today.getMonth()} nam ${today.getFullYear()}`
    })
})

app.listen(port, () => {
    console.log('App listening on port', port);
});

app.use(courseRouter);
app.use(reviewRouter);
