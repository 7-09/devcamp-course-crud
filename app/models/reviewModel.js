// import app thu vien mongoose
const mongoose = require("mongoose");

// Khai bao class schema cua mongoose
const schema = mongoose.Schema;

//Khai bao review schema
const reviewSchema = new mongoose.Schema({
    // co the khai bao hoac khong 
    _id : {
        type: mongoose.Types.ObjectId,
        required: true
    },
    stars : {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("reviews", reviewSchema);
