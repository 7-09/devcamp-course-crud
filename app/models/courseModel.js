//Import thu vien mongoose
const mongoose = require("mongoose");

//Khai bao class Schema cua thu vien mongoose
const Schema = mongoose.Schema;

//Khai bao ourse schema 
const courseSchema = new mongoose.Schema({
    title : {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
}, {
    timestamps: true
});

module.exports = mongoose.model("course", courseSchema)