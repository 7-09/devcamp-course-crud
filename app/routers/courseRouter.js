const express = require("express");

const router = express.Router();

const {createCourse, getAllCourse, getCourseById, updateCourseById, deleteCourseById} = require ("../controllers/courseController")

router.get("/courses", getAllCourse);

router.get('/courses/:courseId', getCourseById);

router.post("/courses", createCourse);

router.put('/courses/:courseId', updateCourseById);

router.delete('/courses/:courseId', deleteCourseById);

module.exports = router;