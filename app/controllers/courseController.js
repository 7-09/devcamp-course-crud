// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Course Model
const courseModel = require("../models/courseModel");

// Create course
const createCourse = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let body = req.body;

    // B2: Validate dữ liệu
    if(!body.bodyTitle) {
        return res.status(400).json({
            message: "Title is required!"
        })
    }

    if(!Number.isInteger(body.bodyStudent) || body.bodyStudent < 0) {
        return res.status(400).json({
            message: "No Student is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newCourseData = {
        _id: mongoose.Types.ObjectId(),
        title: body.bodyTitle,
        description: body.bodyDescription,
        noStudent: body.bodyStudent
    }

    courseModel.create(newCourseData, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(201).json({
            message: "Create successfully",
            newCourse: data
        })
    })
}

// Get all course 
const getAllCourse = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    courseModel.find((error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Get all courses successfully",
            courses: data
        })
    })
}

// Get course by id
const getCourseById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let courseId = req.params.courseId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            message: "Course ID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    courseModel.findById(courseId, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(201).json({
            message: "Get course successfully",
            course: data
        })
    })
}

// Update course by id
const updateCourseById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let courseId = req.params.courseId;
    let body = req.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            message: "Course ID is invalid!"
        })
    }

    // Bóc tách trường hợp undefied
    if(body.bodyTitle !== undefined && body.bodyTitle == "") {
        return res.status(400).json({
            message: "Title is required!"
        })
    }

    if(body.bodyStudent !== undefined && (!Number.isInteger(body.bodyStudent) || body.bodyStudent < 0)) {
        return res.status(400).json({
            message: "No Student is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let courseUpdate = {
        title: body.bodyTitle,
        description: body.bodyDescription,
        noStudent: body.bodyStudent
    };

    courseModel.findByIdAndUpdate(courseId, courseUpdate, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Update course successfully",
            updatedCourse: data
        })
    })
}

// Delete course by id
const deleteCourseById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let courseId = req.params.courseId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            message: "Course ID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(204).json({
            message: "Delete course successfully"
        })
    })
}

// Export Course controller thành 1 module
module.exports = {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
}



